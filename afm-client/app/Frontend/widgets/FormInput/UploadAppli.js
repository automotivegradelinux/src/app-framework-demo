
/* 
 * Copyright (C) 2015 "IoT.bzh"
 * Author "Fulup Ar Foll"
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details..
 * 
 * Reference:
 *   https://developer.mozilla.org/en/docs/Web/API/FileReader 
 *   https://developer.mozilla.org/en-US/docs/Using_files_from_web_applications#Using_hidden_file_input_elements_using_the_click%28%29_method
 *   https://uncorkedstudios.com/blog/multipartformdata-file-upload-with-angularjs
 *   https://www.terlici.com/2015/05/16/uploading-files-locally.html
 *   https://github.com/nervgh/angular-file-upload/blob/master/src/services/FileUploader.js
 *   https://stuk.github.io/jszip/documentation/howto/read_zip.html
 *   http://onehungrymind.com/zip-parsing-jszip-angular/
 *   http://stackoverflow.com/questions/15341912/how-to-go-from-blob-to-arraybuffer
 *   
 *   Bugs: zip file sent even when flag as invalid 
 */

 

(function() {
'use strict';

var tmplAppli = '<input type="file" name="{{name}}-input" onchange="angular.element(this).scope().UpLoadFile(this.files)" accept="{{mimetype}}" style="display:none">'+
            '<div class="upload-file" ng-click="imgClicked()">' +
            '<i class="{{icon}}"></i> <span>{{label}}</span>' +
            '<range-slider ng-show="!noslider" id="{{name}}-slider" automatic=true inithook="SliderInitCB"></range-slider>' +
            '</div>';
    
var tmplModal = '<span class="modal-text">Upload Application <b>{{appname}}</b> ?</span>' +
            '<div>'+
            '<img ng-src="{{icon}}">' +
            '<submit-button icon="fi-x" label="Cancel" clicked="abandon"></submit-button>'+
            '<submit-button icon="fi-like" label="Install" clicked="success"></submit-button> ' +
            '</div>';
    
var tmplError = '<span class="modal-text">Invalid Application <b>{{appname}}</b> ?</span>' +
            '<div>'+
            '<img ng-src="{{icon}}">' +
            '<submit-button icon="fi-x" label="Close" clicked="abandon"></submit-button>'+
            '</div>';
    

// Service Create xform insert files in and Post it to url
function LoadFileSvc (scope, files, fileCB) {
    var xmlReq = new XMLHttpRequest();
    var xform  = new FormData();
    
    // Update slider during Upload
    xmlReq.upload.onprogress = function (event) {
        var progress = Math.round(event.lengthComputable ? event.loaded * 100 / event.total : 0);
        if (scope.slider) scope.slider.setValue (progress);
    };

    // Upload is finish let's notify controler callback
    xmlReq.onload = function () {
        scope.divElem.addClass ("success");
        scope.divElem.removeClass ("error");
        var response ={
            status : xmlReq.status,
            headers: xmlReq.getAllResponseHeaders() 
        };
        scope.callback (response);
    };

    xmlReq.onerror = function () {
        scope.divElem.addClass ("error");
        scope.divElem.removeClass ("success");
    };

    xmlReq.onabort = function () {
        scope.divElem.addClass ("error");
        scope.divElem.removeClass ("success");
        var response ={
            status : xmlReq.status,
            headers: xmlReq.getAllResponseHeaders() 
        };
        scope.callback (response);
    };
    
    this.postfile = function(posturl) { 
        // everything looks OK let's Post it
        xmlReq.open("POST", posturl , true);
        xmlReq.send(xform);
    };

    for (var i = 0; i < files.length; i++) {
        this.file = files[i];
        // Unknow Type !!! if (!this.file.type.match(scope.mimetype)) continue;

        console.log ("Selected file=" + this.file.name + " size="+ this.file.size/1024 + " Type="+ this.file.type);
        
        this.basename= this.file.name.split('/').reverse()[0];

        // File to upload is too big
        if (isNaN(this.file.size) || this.file.size > scope.maxsize*1024) {
            setTimeout (fileCB,100);  // On error asynchronous callback without argument
            
        } else {

            // If File is readable let's read it
            var reader = new FileReader();
            reader.readAsArrayBuffer(this.file);
            reader.onload = fileCB;

            // if everything is OK let's add file to xform
            xform.append(scope.name, this.file, this.file.name);
        }
    }
}

angular.module('UploadFiles',['AppConfig', 'ModalNotification', 'RangeSlider'])

.directive('uploadAppli', function(AppConfig,  JQemu, Notification, ModalFactory, $timeout) {
    function mymethods(scope, elem, attrs) {
        
        // get widget image handle from template
        scope.inputElem  = elem.find('input');
        scope.divElem    = elem.find('div');
        
        // Image was ckick let's simulate an input (file) click
        scope.imgClicked = function () {
            scope.inputElem[0].click(); // Warning Angular TriggerEvent does not work!!!
        };
        
        // Slider control handle registration after creation
        scope.SliderInitCB=function (slider) {
           scope.slider= slider; 
        };
        
        // Upload is delegated to a shared function
        scope.UpLoadFile=function (files) {
            var handle; 
            var appicon;
            var template;
            
            var success = function() {
                // This Looks OK let's Post Xform/File
                handle.postfile(attrs.posturl + "?token=" + AppConfig.session.token);

                scope.modal.deactivate();
                $timeout (function() {scope.modal.destroy();}, 1000);
            };
            
            var abandon = function() {
                scope.modal.deactivate();
                $timeout (function() {scope.modal.destroy();}, 1000);
            };
                       
            var readerCB = function (upload) {
               
                // File upload fail handle error
                if (! upload) {
                    if (handle.file.size > scope.maxsize*1024) {
                        appicon = scope.istoobig;
                        template= tmplError;
                    }
                    
                    if (isNaN(handle.file.size)) {
                        appicon = scope.isnotvalid; 
                        template= tmplError;
                    }
                                        
                } else {

                    var zipapp = new JSZip (upload.target.result);
                    var thumbnail = zipapp.file("icon_128.png");

                    // Check is we have a thumbnail within loaded Zipfile
                    if (!thumbnail) {
                        console.log ("This is not a valid Application Framework APP");
                        appicon = scope.isnotvalid;
                        template= tmplError;
                        
                    } else {
                        //scope.imgElem[0].src = window.URL.createObjectURL(new Blob([thumbnail.asArrayBuffer()], {type: "image"}));
                        appicon = window.URL.createObjectURL(new Blob([thumbnail.asArrayBuffer()], {type: "image"}));
                        template = tmplModal;
                    }
                }
                
                // reference http://foundation.zurb.com/apps/docs/#!/angular-modules
                var config = {
                    animationIn: 'slideInFromTop',
                    contentScope: {
                        success : success,
                        abandon : abandon,
                        icon    : appicon,
                        appname : handle.basename
                    }, template : template
                }; 
                // Popup Modal to render application data
                scope.modal = new ModalFactory(config);
                scope.modal.activate ();
            };
            
            // Load file within browser and if OK call readerCB
            handle = new LoadFileSvc (scope, files, readerCB);
            console.log (handle);
        };

        // Initiallize default values from attributes values
        scope.name= attrs.name || 'appli';
        scope.category= attrs.category  || 'appli';
        scope.mimetype= (attrs.accept || '.wgt');
        scope.maxsize = attrs.maxsize || 100000; // default max size 100MB
        scope.regexp  = new RegExp (attrs.accept+ '.*','i');
        scope.icon    = attrs.icon || 'fi-upload';
        scope.label   = attrs.label || 'Upload';
        
        if (attrs.thumbnail) scope.isnotvalid= AppConfig.paths[scope.category] +  attrs.isnotvalid;
        else  scope.isnotvalid=AppConfig.paths[scope.category] + 'w3c-widget.png';

        if (attrs.istoobig) scope.istoobig= AppConfig.paths[scope.category] +  attrs.istoobig;
        else  scope.istoobig=AppConfig.paths[scope.category] + 'istoobig.png';
        scope.noslider = attrs.noslider || false;

        if (!attrs.posturl) throw new TypeError('file-upload %s posturl=/api/xxxx/xxxx required', scope.attrs);            
    }
    return {
        restrict: 'E',
        template: tmplAppli,
        link: mymethods,
        scope: {
            callback : '='
        }
    };
    
});

console.log ("UploadFile Loaded");
})();
