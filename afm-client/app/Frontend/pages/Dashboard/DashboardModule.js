(function() {
'use strict';

// WARNING: make sure than app/frontend/services/AppConfig.js match your server

// list all rependencies within the page + controler if needed
angular.module('DashboardModule', ['SubmitButton', 'TokenRefresh', 'AppliButton'])

  .controller('DashboardController', function (AppCall, Notification) {
        var scope = this; // I hate JavaScript
        scope.uuid   ="none";
        scope.token  ="none";
        scope.session="none";
        scope.status ="err-no";
        scope.appliIDs =[]; // array to hold applications ID
        scope.appliStore={}; // array to hold applications json description

       
        scope.AppliCB = function(appliID, action, response) {
                // Action is done within Widget Controller only update debug UI zone
                scope.request  = action; 
                scope.errcode  = response.status;
                if (response.data) scope.response = response.data;
                
                // On app was removed let's update runnable list
                if (action === "uninstall")  scope.GetRunnables();
        };
        
        scope.GetRunnables = function() {
            console.log ("Dashboard GetRunnables");
            
            AppCall.get ("afm-main", "runnables", {/*query*/}, function(jresp, errcode) {
                
                // update debug UI zone
                scope.request  = "/api/afm-main/runnable"; 
                scope.response = jresp.response;
                scope.errcode  = jresp.request.status;

                // Check if this is a response from AGL application framework binder
                if (jresp.jtype !== "afb-reply") {
                  Notification.error ({message: "Invalid Respond to /opa/afm-main/runnable response.data="+response.data, delay: 5000}); 
                  return;
                }

                // Check for success
                if (jresp.request.status !== "success") {
                    Notification.error ({message: "afm-main/runnable" + jresp.request.info, delay: 5000});
                    return;
                }
                                   
                
                // loop on runnable application to prepare for display
                var  appliIDs=[];
                var  runnables = jresp.response.runnables;
                for (var idx=0; idx < runnables.length; idx ++) {
                    appliIDs[idx] = runnables [idx].id;
                    scope.appliStore [runnables [idx].id] =  runnables [idx];
                }
                scope.appliIDs = appliIDs; // avoid partial update to limit UI refresh
                
            });            
        };
        
        scope.FileUploaded = function (response) {
            console.log ("file Uploaded");
            // Cannot display post results as GetRunnable will overload them aynchronously
            scope.request  = "/api/afm-main/install"; 
            scope.response = response.headers;
            scope.errcode  = response.status;
            
            // everything looks OK update app list
            scope.GetRunnables();
        };
        
        scope.AutoStart = function () {
            console.log ("AutoStart requesting Apps list");
            scope.GetRunnables();
        };
          
   });

console.log ("Dashboard Controller Loaded");
})(); 
